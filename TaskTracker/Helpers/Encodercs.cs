﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.Helpers
{
    public static class Encodercs
    {
        public static string GenerateSalt(int length)
        {
            string toReturn = String.Empty;
            const string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var randNumber = new Random();
            var chars = new char[length];
            var allowedCharCount = allowedChars.Length;

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[Convert.ToInt32((allowedChars.Length) * randNumber.NextDouble())];
            }

            toReturn = new string(chars);

            return toReturn;
        }

        public static string EncodePassword(string password,  string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dest = new byte[src.Length + bytes.Length];
            System.Buffer.BlockCopy(src, 0, dest, 0, src.Length);
            System.Buffer.BlockCopy(bytes, 0, dest, src.Length, bytes.Length);
            HashAlgorithm hashAlg = SHA1.Create();
            byte[] inArray = hashAlg.ComputeHash(dest);

            return EncodePasswordMd5(Convert.ToBase64String(inArray));
        }

        public static string EncodePasswordMd5(string pass) //Encrypt using MD5    
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            //Instantiate MD5CryptoServiceProvider, get bytes for original password and compute hash (encoded password)    
            md5 = MD5.Create();

            originalBytes = ASCIIEncoding.Unicode.GetBytes(pass);
            encodedBytes = md5.ComputeHash(originalBytes);
            //Convert encoded bytes back to a 'readable' string    
            return BitConverter.ToString(encodedBytes);
        }
    }
}
