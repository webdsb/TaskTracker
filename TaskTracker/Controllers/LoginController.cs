using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using TaskTracker.Models.Account;
using Microsoft.Extensions.Configuration;
using TaskTracker.Models.Settings;
using Microsoft.Extensions.Options;

namespace TaskTracker.Controllers
{
    public class LoginController : Controller
    {
        private readonly IOptions<Settings> _settings;
        private string connString;
        private Service serviceLayer;

        public LoginController(IOptions<Settings> connection)
        {
            _settings = connection;
            connString = _settings.Value.DefaultConnection;
            serviceLayer = new Service(connString);
        }
        public IActionResult Index()
        {            
            return View();
        }

        public ActionResult Login()
        {
            LoginViewModel vm = new LoginViewModel();

            return View(vm);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(LoginViewModel model)
        {
            serviceLayer.Register(model.UserName, model.Password, model.Email);
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if (serviceLayer.ValidUser(model.UserName, model.Password))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,model.UserName, ClaimValueTypes.String)
                };

                var userIdentity = new ClaimsIdentity(claims, "Cookie");
                var userPrinicipal = new ClaimsPrincipal(userIdentity);

                await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", userPrinicipal,
                    new Microsoft.AspNetCore.Http.Authentication.AuthenticationProperties
                    {
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20),
                        IsPersistent = false,
                        AllowRefresh = false
                    });

                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Login", "Login");

        }
    }
}