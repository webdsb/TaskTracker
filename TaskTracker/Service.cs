﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskTracker
{
    public class Service
    {
        private string connString;
        private DataAccess.DataAccess da;

        public Service(string connString)
        {
            this.connString = connString;
            da = new DataAccess.DataAccess(connString);
        }

        public bool ValidUser(string userName, string password)
        {
            bool toReturn = false;

            string salt = da.GetSalt(userName);
            if (!String.IsNullOrEmpty(salt))
            {
                string salted = Helpers.Encodercs.EncodePassword(password, salt);

                toReturn = da.ValidUser(userName, salted);
            }           

            return toReturn;
        }

        public void Register(string userName, string password, string email)
        {
            string salt = Helpers.Encodercs.GenerateSalt(10);
            string salted = Helpers.Encodercs.EncodePassword(password, salt);
            da.Register(userName, email, salt, salted);
        }
    }
}
