﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;

namespace TaskTracker.DataAccess
{
    public class DataAccess
    {       
        private MySqlConnection connection = null;
        private string connString;

        public DataAccess(string connString)
        {
            this.connString = connString;    
        }

        private void GetConnection()
        {
            connection = new MySqlConnection()
            {                
                ConnectionString = connString
            };
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
        }

        private void CloseConnection()
        {
            if (null != connection)
            {
                if (connection.State != System.Data.ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }
        public string GetSalt(string userName)
        {
            string toReturn = String.Empty;

            try
            {
                GetConnection();

                MySqlCommand command = new MySqlCommand()
                {
                    Connection = connection,
                    CommandText = $"SELECT * FROM Users WHERE UserID='{userName}'"
                };
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    toReturn = reader.GetString(reader.GetOrdinal("Salt"));
                }

            }
            catch (Exception)
            { }
            finally
            {
                CloseConnection();
            }

            return toReturn;
        }
        public bool ValidUser(string userName, string salted)
        {
            bool toReturn = false;

            try
            {
                GetConnection();

                MySqlCommand command = new MySqlCommand()
                {
                    Connection = connection,
                    CommandText = $"SELECT * FROM Users WHERE UserID='{userName}' AND Hash='{salted}'"
                };
                MySqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                    toReturn = true;
            }
            catch (Exception)
            { }
            finally
            {
                CloseConnection();
            }

            return toReturn;
        }

        public void Register(string userName, string email, string salt, string salted)
        {
            try
            {
                GetConnection();
                MySqlCommand command = new MySqlCommand()
                {
                    Connection = connection,
                    CommandText = $"INSERT INTO Users(Email, IsActive, Salt, Hash,  UserID) VALUES('{email}', 0, '{salt}', '{salted}', '{userName}')"
                };
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {

            }
            finally
            {
                CloseConnection();
            }

            
        }
    }


}
